<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2014/9/16
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title></title>
</head>
<body>
    <%@include file="hometop.jsp"%>
    <c:if test="${shangjia_error != null}">
        <table align="center">
            <tr>
                <td>
                    <h2>${shangjia_error}</h2>
                </td>
            </tr>
            <c:remove var="shangjia_error"></c:remove>
        </table>
    </c:if>
</body>
</html>
