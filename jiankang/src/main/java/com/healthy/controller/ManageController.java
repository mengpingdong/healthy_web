package com.healthy.controller;

import com.healthy.domain.Manage;
import com.healthy.exception.DaoException;
import com.healthy.exception.ServiceException;
import com.healthy.service.ManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by dell on 2014/9/9.
 */
@Controller
@RequestMapping("/Manage")
public class ManageController {
    @Autowired
    private ManageService manageService;
    @Autowired
    private HttpSession httpSession;

    /**
     *  跳转到管理员登录界面
     * @return
     */
    @RequestMapping("/LoginPage")
    public String ManageLoginPage(){
        return "manageLoginPage";
    }

    /**
     * 管理员表单登录验证并跳转
     * @param manage
     * @param model
     * @return
     * @throws ServiceException
     * @throws DaoException
     */
    @RequestMapping(value = "/Login",method = RequestMethod.POST)
    public String manageLoginCheck(Manage manage,Model model) throws ServiceException, DaoException {
        Manage manageList = manageService.findManageByName(manage.getManageName());
        if (!(manageList == null) && manageList.getManagePwd().equals(manage.getManagePwd())) {
            httpSession.setAttribute("sessionManage", manageList);
            return "manageLoginPage";
        }
        else{
            model.addAttribute("manage_login_error", "对不起，用户名或密码错误！");
            return "manageLoginPage";
        }
    }
    /**
     * 管理员注销
     * @return
     */
    @RequestMapping(value = "/Logout")
    public String manageLogout(){
        Object sessionManage = httpSession.getAttribute("sessionManage");

        System.out.print(sessionManage);

        if(sessionManage == null){
            httpSession.setAttribute("logout_error","请先登录！");
        }
          httpSession.removeAttribute("sessionManage");
          return "logout";
    }

    /**
     * 得到所有管理员，跳转到showManager页面并显示
     * @param model
     * @return
     * @throws ServiceException
     * @throws DaoException
     */
    @RequestMapping( value = "/managerManage")
    public String manageManage(Model model) throws ServiceException, DaoException {
        String hql = "from Manage";
        List<Manage> manageList = manageService.findAllManage(hql);
        model.addAttribute("manageList",manageList);
        return "showManager";
    }

    /**
     * 返回到修改管理员界面
     * @param manage
     * @param model
     * @return
     * @throws ServiceException
     * @throws DaoException
     */
    @RequestMapping(value = "/updatePage")
    public String manageUpdatePage(Manage manage,Model model) throws ServiceException, DaoException {
        String mangeName = manage.getManageName();
        Manage manage1 = manageService.findManageByName(mangeName);
        model.addAttribute("updatedManage",manage1);
        return "updateManagePage";
    }

    /**
     * 更改信息并跳转
     * @param manage
     * @param response
     * @throws ServiceException
     * @throws DaoException
     * @throws IOException
     */
    @RequestMapping(value = "/update")
    public void updateManage(Manage manage,HttpServletResponse response) throws ServiceException, DaoException, IOException {
        manageService.updateManager(manage);
        response.sendRedirect("/Manage/managerManage");
    }
    @RequestMapping(value = "/Uncheck")
    public String uncheck(Model model){
        return "uncheck";
    }

}
