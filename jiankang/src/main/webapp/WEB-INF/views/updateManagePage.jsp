<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2014/9/14
  Time: 10:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>修改管理员信息</title>
    <script language="JavaScript">
        function Check(){
            if(document.updateForm.manageName.value==""){
                alert("请填写管理员名字！！！");
                document.updateForm.manageName.focus();
                return false;
            }
            if(document.updateForm.managePwd.value==""){
                alert("请填写密码！！！");
                document.updateForm.managePwd.focus();
                return false;
            }
            document.updateForm.submit();
        }
    </script>
</head>
<body>
    <%@include file="manageTop.jsp"%>
    <form name="updateForm" action="<c:url value="/Manage/update"/>" method="post">
        <table align="center" width="40%" height="60%">
            <tr>
                <td align="left">
                    管理员ID：
                <td align="left">
                    <input type="hidden" name="manageId" value="${updatedManage.manageId}">
                </td>
                <td align="left"> <c:out value="${updatedManage.manageId}"/></td>
                </td>
            </tr>
                

            <tr>
                <td align="left">
                    管理员名字：
                </td>
                <td align="left">
                    <input type="text" name="manageName" value="${updatedManage.manageName}">
                </td>
            </tr>
            <tr>
                <td align="left"> 管理员级别：</td>
                    <c:if test="${updatedManage.manageLevel==1}">
                        <td>
                            <input type="radio" name="manageLevel" value="1" checked>超级管理员
                        </td>
                        <td>
                            <input type="radio" name="manageLevel" value="0">普通管理员
                        </td>
                    </c:if>
                    <c:if test="${updatedManage.manageLevel==0}">
                        <td>
                            <input type="radio" name="manageLevel" value="1">超级管理员
                        </td>
                        <td>
                            <input type="radio" name="manageLevel" value="0" checked>普通管理员
                        </td>
                    </c:if>
            </tr>
            <tr>
                <td align="left">管理员密码：</td>
                <td>
                    <input type="password" name="managePwd" value="${updatedManage.managePwd}">
                </td>
            </tr>
            <tr align="center">
                <td align="left">
                    <input type="button" value="提交" onclick="Check()">
                </td>
                <td align="left">
                    <input type="reset" value="重置">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
