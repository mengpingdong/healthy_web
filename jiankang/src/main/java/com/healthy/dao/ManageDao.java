package com.healthy.dao;

import com.healthy.domain.Manage;
import com.healthy.exception.DaoException;

import java.util.List;

/**
 * Created by dell on 2014/9/9.
 */
public interface ManageDao {
    public void saveManager(Manage manage) throws DaoException;

    public void deleteManager(Manage manage) throws DaoException;

    public void updateManager(Manage manage) throws DaoException;

    public Manage findManagerByName(String manageName) throws DaoException;

    public List findAllManager(String hql) throws DaoException;

}
