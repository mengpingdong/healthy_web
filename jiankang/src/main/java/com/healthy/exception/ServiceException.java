package com.healthy.exception;

/**
 * Created by dell on 2014/9/8.
 */
public class ServiceException extends Exception {

    public ServiceException(){
        super();
    }
    public ServiceException(String string){
        super(string);
    }
}
