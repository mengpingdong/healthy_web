package com.healthy.util;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by dell on 2014/9/11.
 */
public class SetCharacterFilter implements Filter {
    protected String encoding = null;
    protected FilterConfig filterConfig = null;
    protected boolean ignore = true;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        this.encoding = filterConfig.getInitParameter("encoding");
        String value = filterConfig.getInitParameter("ignore");
        if (value == null)
            this.ignore = true;
        else if (value.equalsIgnoreCase("true"))
            this.ignore = true;
        else if (value.equalsIgnoreCase("yes"))
            this.ignore = true;
        else
            this.ignore = false;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (ignore || (servletRequest.getCharacterEncoding() == null)) {
            String encoding = selectEncoding(servletRequest);
            if (encoding != null)
                servletRequest.setCharacterEncoding(encoding);
        }

        // Pass control on to the next filter
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        this.encoding = null;
        this.filterConfig = null;
    }

    protected String selectEncoding(ServletRequest request) {

        return (this.encoding);

    }

}
