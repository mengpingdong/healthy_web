<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2014/10/1
  Time: 15:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>商家审核</title>
</head>
<body>
    <%@include file="manageTop.jsp"%>
    <center>
        <c:choose>
            <c:when test="${sessionManage==null}">
                <h2>对不起，请先登录！</h2>
            </c:when>
            <c:when test="${sessionManage!=null}">
                <table align="center" width="100%" height="100%" cellspacing="1" border="2">
                    <tr bgcolor="#f0f8ff" align="center">
                        <td>商家编号</td>
                        <td>商家姓名</td>
                        <td>商家Email</td>
                        <td>商家介绍</td>
                        <td>固定电话</td>
                        <td>移动电话</td>
                        <td>商家地址</td>
                        <td>商家邮编</td>
                        <td>操作</td>
                    </tr>
                    <c:forEach items="${shangjiaList_attribute}" var="shangjiaList_attribute" varStatus="status">
                        <tr bgcolor="#f0f8ff" align="center">
                            <td>
                                <c:out value="${shangjiaList_attribute.shangjiaId}"></c:out>
                            </td>
                            <td>
                                <c:out value="${shangjiaList_attribute.shangjiaName}"></c:out>
                            </td>
                            <td>
                                <c:out value="${shangjiaList_attribute.shangjiaEmail}"></c:out>
                            </td>
                            <td>
                                <c:out value="${shangjiaList_attribute.introduce}"></c:out>
                            </td>
                            <td>
                                <c:out value="${shangjiaList_attribute.fixedPhone}"></c:out>
                            </td>
                            <td>
                                <c:out value="${shangjiaList_attribute.mobilePhone}"></c:out>
                            </td>
                            <td>
                                <c:out value="${shangjiaList_attribute.shangjiaAddress}"></c:out>
                            </td>
                            <td>
                                <c:out value="${shangjiaList_attribute.shangjiaZip}"></c:out>
                            </td>
                            <td>
                                <a href="<c:url value=""/> ">通过</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
        </c:choose>
    </center>
</body>
</html>
