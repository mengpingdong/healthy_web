package com.healthy.exception;

/**
 * Created by dell on 2014/9/8.
 */
public class DaoException extends Exception {

    public DaoException(){
        super("无法与数据库联系，请与管理员联系！！！");
    }
    public DaoException(String string){
        super(string);
    }
}

