package com.healthy.dao.daoImpl;

import com.healthy.dao.BaseDao;
import com.healthy.dao.ManageDao;
import com.healthy.domain.Manage;
import com.healthy.exception.DaoException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by dell on 2014/9/9.
 */
@Repository("managedaoimpl")
public class ManageDaoImpl extends HibernateDaoSupport implements ManageDao {

    @Resource( name = "sessionFactory")
    public void setMySessionFactory(SessionFactory sessionFactory){
        super.setSessionFactory(sessionFactory);
    }
    @Autowired
    private BaseDao baseDao;
    @Override
    public void saveManager(Manage manage) throws DaoException {
        baseDao.sava(manage);
    }

    @Override
    public void deleteManager(Manage manage) throws DaoException {
        baseDao.delete(manage);
    }

    @Override
    public void updateManager(Manage manage) throws DaoException {
        baseDao.update(manage);
    }

    @Override
    public Manage findManagerByName(String manageName) throws DaoException {
        final String hql = "from Manage where manageName = ?";
        Query query = getSession().createQuery(hql);
        query.setParameter(0,manageName);
        List<Manage> manageList = query.list();
        if(manageList.size()==0||manageList==null){
            return null;
        }
        return manageList.get(0);
    }

    @Override
    public List<Manage> findAllManager(String hql) throws DaoException {
        return baseDao.find(hql);
    }



}
