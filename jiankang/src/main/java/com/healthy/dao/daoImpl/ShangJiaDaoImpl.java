package com.healthy.dao.daoImpl;

import com.healthy.dao.BaseDao;
import com.healthy.dao.ShangJiaDao;
import com.healthy.domain.Shangjia;
import com.healthy.exception.DaoException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by dell on 2014/9/16.
 */
@Repository("shangJiaDaoImpl")
public class ShangJiaDaoImpl extends HibernateDaoSupport implements ShangJiaDao {

    @Resource( name="sessionFactory")
    private void setMySessionFactory(SessionFactory sessionFactory){
        super.setSessionFactory(sessionFactory);
    }
    @Autowired
    private BaseDao baseDao;
    @Override
    public void savaShangJia(Shangjia shangjia) throws DaoException {
        baseDao.sava(shangjia);
    }

    @Override
    public void deleteShangJia(Shangjia shangjia) throws DaoException {
        baseDao.delete(shangjia);
    }

    @Override
    public void updateShangJia(Shangjia shangjia) throws DaoException {
        baseDao.update(shangjia);
    }

    @Override
    public Shangjia findShangJiaByName(String shangJiaName) throws DaoException {
        final String hql = "from Shangjia where shangjiaName = ?";
        Query query = getSession().createQuery(hql);
        query.setParameter(0,shangJiaName);
        List<Shangjia> shangjiaList = query.list();
        if(shangjiaList.size()==0 ||shangjiaList== null)
            return null;
        return shangjiaList.get(0);
    }

    @Override
    public List findAllShangJia(String hql) throws DaoException {
        return baseDao.find(hql);
    }

    /**
     * 得到所有没有审核的商家
     * @param hql
     * @return
     * @throws DaoException
     */
    @Override
    public List findAllShangJiaUnchecked(String hql) throws DaoException {
        List<Shangjia> shangjiaList = baseDao.find(hql);
        if(shangjiaList.size()!=0){
            return shangjiaList;
        }
        return null;
    }
}
