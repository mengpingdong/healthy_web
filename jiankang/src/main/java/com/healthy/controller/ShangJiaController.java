package com.healthy.controller;

import com.healthy.domain.Shangjia;
import com.healthy.exception.DaoException;
import com.healthy.exception.ServiceException;
import com.healthy.service.ShangJiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by dell on 2014/9/16.
 */
@Controller
@RequestMapping("/ShangJia")
public class ShangJiaController {
    @Autowired
    private ShangJiaService shangJiaService;
    @Autowired
    private HttpSession httpSession;


    /**
     * 商家登录验证，并返回相关页面
     * @param shangjia
     * @param model
     * @return
     * @throws ServiceException
     * @throws DaoException
     */
    @RequestMapping(value = "/Login",method = RequestMethod.POST)
    public String Login(Shangjia shangjia,Model model) throws ServiceException, DaoException {
        Shangjia shangjia1 = shangJiaService.findShangJiaByName(shangjia.getShangjiaName());
        if (!(shangjia1 == null) && shangjia1.getShangjiaPwd().equals(shangjia.getShangjiaPwd())) {
            httpSession.setAttribute("shangjia_session", shangjia1);
            model.addAttribute("shangjia_error", "登录成功！");
            return "LoginSuccess";
        }
        else{
            model.addAttribute("shangjia_error","用户名或密码错误！！！");
            return "home";
        }
    }

    /**
     * 商家注销
     * @return
     */
    @RequestMapping(value = "/Logout")
    public String ShangJiaLogout(){
        Object shangjia_session = httpSession.getAttribute("shangjia_session");
        if (shangjia_session == null){
            httpSession.setAttribute("logout_error","对不起，请先登录！");
        }
        httpSession.removeAttribute("shangjia_session");
        return "logout";
    }

    /**
     *  model必须添加一个shangjia对象的属性，用于注册表单（registerPage.jsp）使用
     * @param model
     * @return
     * @throws DaoException
     * @throws ServiceException
     */
    //TODO

    @RequestMapping(value = "/registerPage")
    public String ShangJiaRegister(Model model) throws DaoException,ServiceException{
        model.addAttribute(new Shangjia());
        return "registerPage";
    }

    /**
     * 验证商家并向数据库插入数据，跳转到相应页面
     * @param shangjia
     * @param bindingResult
     * @return
     * @throws ServiceException
     * @throws DaoException
     */
    //TODO
    @RequestMapping(method = RequestMethod.POST,value = "/addShangJia")
    public String addShangJiaFromForm(@Valid Shangjia shangjia,BindingResult bindingResult) throws ServiceException, DaoException {
        if (bindingResult.hasErrors()){
            return "registerPage";
        }
        shangJiaService.saveShangJia(shangjia);
        httpSession.setAttribute("shangjia_check_message","注册成功！！！请等待管理员审核！！！");
        return "shangJiaCheck";
    }

    /**
     * 得到所有未审核的商家
     * @param model
     * @return
     * @throws ServiceException
     * @throws DaoException
     */
    //TODO
    @RequestMapping(value = "/allUncheckedShangJia")
    public String getAllUncheckedShangJia(Model model) throws ServiceException, DaoException {
        final String hql = "from Shangjia where isCheck = null";
        List<Shangjia> shangjiaList = shangJiaService.findAllShangJiaUncheckd(hql);
        if(shangjiaList.size()==0&&shangjiaList==null){
           model.addAttribute("shangjiaList_attribute","没有未审核的商家！！！");
           return "noUncheckShangjia";
        }
        {
            model.addAttribute("shangjiaList_attribute",shangjiaList);
            return "allUncheckShangjia";
        }
    }
}
