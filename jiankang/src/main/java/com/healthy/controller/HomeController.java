package com.healthy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dell on 2014/9/9.
 */
@Controller
public class HomeController {
    @RequestMapping(value = "home",method = RequestMethod.GET)
    public String renderPage(Model model){
        return "home";
    }
}
