package com.healthy.dao;

import com.healthy.domain.BaseDomain;
import com.healthy.exception.DaoException;

import java.util.List;

/**
 * Created by dell on 2014/9/6.
 */
public interface BaseDao {

    public void sava(BaseDomain baseDomain) throws DaoException;

    public void delete(BaseDomain baseDomain) throws DaoException;

    public void update(BaseDomain baseDomain) throws DaoException;

    public List find(String hql) throws DaoException;

    public List find(String hql,Object ... params) throws DaoException;
}
