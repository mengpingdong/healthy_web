package com.healthy.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by dell on 2014/9/6.
 */
public class BaseDomain implements Serializable {

    public String toString(){
        return ToStringBuilder.reflectionToString(this);
    };
}
