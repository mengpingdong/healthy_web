package com.healthy.domain;


import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by dell on 2014/9/16.
 */
@Entity
@Table(name = "shangjia")
public class Shangjia extends BaseDomain{

    private Integer shangjiaId;

    @Size(min = 2, max = 40, message = "用户名必须在 2 到 40 个字符之间！！！")
    private String shangjiaName;

    @Size(min = 6, max = 20, message = "密码必须为 6--20位！！！")
    private String shangjiaPwd;

    private Integer isCheck;

    @Pattern(regexp = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}",message = "Email格式不对！！！")
    private String shangjiaEmail;
    private String shangjiaImage;
    @Size(min = 2,max = 500,message = "商家介绍不能为空，最多为500个字符！！！")
    private String introduce;
    private String fixedPhone;


    private String mobilePhone;


    @Size(max = 200,message = "最多只能输入200个字符！！！")
    private String shangjiaAddress;

    private String shangjiaZip;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shangjia_id", nullable = false, insertable = true, updatable = true)
    public Integer getShangjiaId() {
        return shangjiaId;
    }

    public void setShangjiaId(Integer shangjiaId) {
        this.shangjiaId = shangjiaId;
    }

    @Basic
    @Column(name = "shangjia_name", nullable = false, insertable = true, updatable = true, length = 50)
    public String getShangjiaName() {
        return shangjiaName;
    }

    public void setShangjiaName(String shangjiaName) {
        this.shangjiaName = shangjiaName;
    }

    @Basic
    @Column(name = "shangjiaPwd", nullable = false, insertable = true, updatable = true, length = 20)
    public String getShangjiaPwd() {
        return shangjiaPwd;
    }

    public void setShangjiaPwd(String shangjiaPwd) {
        this.shangjiaPwd = shangjiaPwd;
    }

    @Basic
    @Column(name = "isCheck", nullable = true, insertable = true, updatable = true)
    public Integer getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }

    @Basic
    @Column(name = "shangjia_email", nullable = true, insertable = true, updatable = true, length = 50)
    public String getShangjiaEmail() {
        return shangjiaEmail;
    }

    public void setShangjiaEmail(String shangjiaEmail) {
        this.shangjiaEmail = shangjiaEmail;
    }

    @Basic
    @Column(name = "shangjia_image", nullable = true, insertable = true, updatable = true, length = 100)
    public String getShangjiaImage() {
        return shangjiaImage;
    }

    public void setShangjiaImage(String shangjiaImage) {
        this.shangjiaImage = shangjiaImage;
    }

    @Basic
    @Column(name = "introduce", nullable = false, insertable = true, updatable = true, length = 500)
    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    @Basic
    @Column(name = "fixed_phone", nullable = true, insertable = true, updatable = true, length = 15)
    public String getFixedPhone() {
        return fixedPhone;
    }

    public void setFixedPhone(String fixedPhone) {
        this.fixedPhone = fixedPhone;
    }

    @Basic
    @Column(name = "mobile_phone", nullable = true, insertable = true, updatable = true, length = 15)
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Basic
    @Column(name = "shangjia_address", nullable = false, insertable = true, updatable = true, length = 200)
    public String getShangjiaAddress() {
        return shangjiaAddress;
    }

    public void setShangjiaAddress(String shangjiaAddress) {
        this.shangjiaAddress = shangjiaAddress;
    }

    @Basic
    @Column(name = "shangjia_zip", nullable = true, insertable = true, updatable = true, length = 8)
    public String getShangjiaZip() {
        return shangjiaZip;
    }

    public void setShangjiaZip(String shangjiaZip) {
        this.shangjiaZip = shangjiaZip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shangjia shangjia = (Shangjia) o;

        if (fixedPhone != null ? !fixedPhone.equals(shangjia.fixedPhone) : shangjia.fixedPhone != null) return false;
        if (introduce != null ? !introduce.equals(shangjia.introduce) : shangjia.introduce != null) return false;
        if (isCheck != null ? !isCheck.equals(shangjia.isCheck) : shangjia.isCheck != null) return false;
        if (mobilePhone != null ? !mobilePhone.equals(shangjia.mobilePhone) : shangjia.mobilePhone != null)
            return false;
        if (shangjiaAddress != null ? !shangjiaAddress.equals(shangjia.shangjiaAddress) : shangjia.shangjiaAddress != null)
            return false;
        if (shangjiaEmail != null ? !shangjiaEmail.equals(shangjia.shangjiaEmail) : shangjia.shangjiaEmail != null)
            return false;
        if (shangjiaId != null ? !shangjiaId.equals(shangjia.shangjiaId) : shangjia.shangjiaId != null) return false;
        if (shangjiaImage != null ? !shangjiaImage.equals(shangjia.shangjiaImage) : shangjia.shangjiaImage != null)
            return false;
        if (shangjiaName != null ? !shangjiaName.equals(shangjia.shangjiaName) : shangjia.shangjiaName != null)
            return false;
        if (shangjiaPwd != null ? !shangjiaPwd.equals(shangjia.shangjiaPwd) : shangjia.shangjiaPwd != null)
            return false;
        if (shangjiaZip != null ? !shangjiaZip.equals(shangjia.shangjiaZip) : shangjia.shangjiaZip != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = shangjiaId != null ? shangjiaId.hashCode() : 0;
        result = 31 * result + (shangjiaName != null ? shangjiaName.hashCode() : 0);
        result = 31 * result + (shangjiaPwd != null ? shangjiaPwd.hashCode() : 0);
        result = 31 * result + (isCheck != null ? isCheck.hashCode() : 0);
        result = 31 * result + (shangjiaEmail != null ? shangjiaEmail.hashCode() : 0);
        result = 31 * result + (shangjiaImage != null ? shangjiaImage.hashCode() : 0);
        result = 31 * result + (introduce != null ? introduce.hashCode() : 0);
        result = 31 * result + (fixedPhone != null ? fixedPhone.hashCode() : 0);
        result = 31 * result + (mobilePhone != null ? mobilePhone.hashCode() : 0);
        result = 31 * result + (shangjiaAddress != null ? shangjiaAddress.hashCode() : 0);
        result = 31 * result + (shangjiaZip != null ? shangjiaZip.hashCode() : 0);
        return result;
    }
}
