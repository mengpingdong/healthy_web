package com.healthy.dao.daoImpl;

import com.healthy.dao.BaseDao;
import com.healthy.domain.BaseDomain;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by dell on 2014/9/8.
 */
@Repository("basedaoimpl")
public class BaseDaoImpl extends HibernateDaoSupport implements BaseDao {

    @Resource(name = "sessionFactory")
    private void setMySessionFactory(SessionFactory sessionFactory){
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public void sava(BaseDomain baseDomain) {
        getHibernateTemplate().save(baseDomain);
    }

    @Override
    public void delete(BaseDomain baseDomain) {
        getHibernateTemplate().delete(baseDomain);
    }

    @Override
    public void update(BaseDomain baseDomain) {
        getHibernateTemplate().update(baseDomain);
    }

    @Override
    public List find(String hql) {
        return getHibernateTemplate().find(hql);
    }

    @Override
    public List find(String hql, Object... params) {
        return getHibernateTemplate().find(hql,params);
    }
}
