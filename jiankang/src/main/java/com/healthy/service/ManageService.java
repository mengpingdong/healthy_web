package com.healthy.service;

import com.healthy.domain.Manage;
import com.healthy.exception.DaoException;
import com.healthy.exception.ServiceException;

import java.util.List;

/**
 * Created by dell on 2014/9/8.
 */
public interface ManageService {
    public void saveManager(Manage manage) throws ServiceException, DaoException;

    public void deleteManager(Manage manage) throws ServiceException, DaoException;

    public void updateManager(Manage manage) throws ServiceException, DaoException;

    public Manage findManageByName(String manageName) throws ServiceException, DaoException;

    public List<Manage> findAllManage(String hql) throws ServiceException, DaoException;
}
