package com.healthy.service.serviceImpl;

import com.healthy.dao.ManageDao;
import com.healthy.domain.Manage;
import com.healthy.exception.DaoException;
import com.healthy.exception.ServiceException;
import com.healthy.service.ManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dell on 2014/9/9.
 */
@Service("manageserviceimpl")
@Transactional
public class ManageServiceImpl implements ManageService{
    @Autowired
    private ManageDao manageDao;
    @Override
    public void saveManager(Manage manage) throws ServiceException, DaoException {
        manageDao.saveManager(manage);
    }

    @Override
    public void deleteManager(Manage manage) throws ServiceException, DaoException {
        manageDao.deleteManager(manage);
    }

    @Override
    public void updateManager(Manage manage) throws ServiceException, DaoException {
        manageDao.updateManager(manage);
    }

    @Override
    public Manage findManageByName(String manageName) throws ServiceException, DaoException {
        return manageDao.findManagerByName(manageName);
    }

    @Override
    public List<Manage> findAllManage(String hql) throws ServiceException, DaoException {
        return manageDao.findAllManager(hql);
    }
}