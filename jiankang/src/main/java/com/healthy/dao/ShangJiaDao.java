package com.healthy.dao;

import com.healthy.domain.Shangjia;
import com.healthy.exception.DaoException;

import java.util.List;

/**
 * Created by dell on 2014/9/16.
 */
public interface ShangJiaDao {

    public void savaShangJia(Shangjia shangjia) throws DaoException;
    public void deleteShangJia(Shangjia shangjia)throws DaoException;
    public void updateShangJia(Shangjia shangjia)throws DaoException;
    public Shangjia findShangJiaByName(String shangJiaName) throws DaoException;
    public List findAllShangJia(String hql) throws DaoException;
    public List findAllShangJiaUnchecked(String hql) throws DaoException;
}
