<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2014/9/18
  Time: 19:42
  To change this template use File | Settings | File Templates.
--%>
<%--   TODO  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>商家注册界面</title>
</head>
<body>
    <div>
        <%@include file="hometop.jsp"%>
    </div>
    <div>
        <sf:form method="POST" modelAttribute="shangjia" action="/ShangJia/addShangJia"
                enctype="multipart/form-data" >
        <fieldset>
            <table cellspacing="0" align="center">
                <tr>
                    <th><sf:label path="shangjiaName">用户名：</sf:label></th>
                    <td><sf:input path="shangjiaName" size ="40"></sf:input><br/>
                        <sf:errors path="shangjiaName" />
                    </td>
                </tr>
                <tr>
                    <th><sf:label path="shangjiaPwd">密码：</sf:label></th>
                    <td><sf:input path="shangjiaPwd" size="20" maxlength="20"></sf:input><br/>
                        <sf:errors path="shangjiaPwd"/>
                    </td>
                </tr>
                <tr>
                    <th><sf:label path="shangjiaEmail">Email:</sf:label></th>
                    <td><sf:input path="shangjiaEmail" size="30"></sf:input><br/>
                        <sf:errors path="shangjiaEmail"/>
                    </td>
                </tr>
                <tr>
                    <th><sf:label path="fixedPhone">固定电话：</sf:label></th>
                    <td><sf:input path="fixedPhone" size="20"></sf:input><br/>
                        <sf:errors path="fixedPhone"/>
                    </td>
                </tr>
                <tr>
                    <th><sf:label path="mobilePhone">移动电话：</sf:label></th>
                    <td>
                        <sf:input path="mobilePhone" size="15"></sf:input><br/>
                        <sf:errors path="mobilePhone"/>
                    </td>
                </tr>
                <tr>
                    <th><sf:label path="introduce">商家介绍：</sf:label></th>
                    <td>
                        <sf:textarea path="introduce" size = "500" rows="7" cols="40"></sf:textarea><br/>
                        <sf:errors path="introduce"/>
                    </td>
                </tr>
                <tr>
                    <th><sf:label path="shangjiaAddress">地址：</sf:label></th>
                    <td>
                        <sf:textarea path="shangjiaAddress" size = "200" rows="7" cols="40"></sf:textarea><br/>
                        <sf:errors path="shangjiaAddress"/>
                    </td>
                </tr>
                <tr>
                    <th><sf:label path="shangjiaZip">邮编：</sf:label></th>
                    <td>
                        <sf:input path="shangjiaZip" size = "15"></sf:input><tr/>
                        <sf:errors path="shangjiaZip"/>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input name="commit" type="submit" value="提交">
                    </td>
                    <td>
                        <input name="reset" type="reset" value="重置">
                    </td>
                </tr>
            </table>
        </fieldset>
        </sf:form>
    </div>
</body>
</html>
