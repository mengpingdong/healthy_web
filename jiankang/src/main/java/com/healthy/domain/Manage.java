package com.healthy.domain;

import javax.persistence.*;

/**
 * Created by dell on 2014/9/11.
 */
@Entity
@Table(name = "manage")
public class Manage extends BaseDomain{
    private Integer manageId;
    private String manageName;
    private String managePwd;
    private Integer manageLevel;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "manage_id", nullable = false, insertable = true, updatable = true)
    public Integer getManageId() {
        return manageId;
    }

    public void setManageId(Integer manageId) {
        this.manageId = manageId;
    }

    @Basic
    @Column(name = "manage_name", nullable = false, insertable = true, updatable = true, length = 50)
    public String getManageName() {
        return manageName;
    }

    public void setManageName(String manageName) {
        this.manageName = manageName;
    }

    @Basic
    @Column(name = "managePwd", nullable = false, insertable = true, updatable = true, length = 20)
    public String getManagePwd() {
        return managePwd;
    }

    public void setManagePwd(String managePwd) {
        this.managePwd = managePwd;
    }

    @Basic
    @Column(name = "manageLevel", nullable = false, insertable = true, updatable = true)
    public Integer getManageLevel() {
        return manageLevel;
    }

    public void setManageLevel(Integer manageLevel) {
        this.manageLevel = manageLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Manage manage = (Manage) o;

        if (manageId != null ? !manageId.equals(manage.manageId) : manage.manageId != null) return false;
        if (manageLevel != null ? !manageLevel.equals(manage.manageLevel) : manage.manageLevel != null) return false;
        if (manageName != null ? !manageName.equals(manage.manageName) : manage.manageName != null) return false;
        if (managePwd != null ? !managePwd.equals(manage.managePwd) : manage.managePwd != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = manageId != null ? manageId.hashCode() : 0;
        result = 31 * result + (manageName != null ? manageName.hashCode() : 0);
        result = 31 * result + (managePwd != null ? managePwd.hashCode() : 0);
        result = 31 * result + (manageLevel != null ? manageLevel.hashCode() : 0);
        return result;
    }
}
