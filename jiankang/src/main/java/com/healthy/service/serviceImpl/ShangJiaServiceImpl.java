package com.healthy.service.serviceImpl;

import com.healthy.dao.ShangJiaDao;
import com.healthy.domain.Shangjia;
import com.healthy.exception.DaoException;
import com.healthy.exception.ServiceException;
import com.healthy.service.ShangJiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dell on 2014/9/16.
 */
@Service("ShangJiaServiceImpl")
public class ShangJiaServiceImpl implements ShangJiaService {
    @Autowired
    private ShangJiaDao shangJiaDao;
    @Override
    public void saveShangJia(Shangjia shangjia) throws DaoException, ServiceException {
        shangJiaDao.savaShangJia(shangjia);
    }

    @Override
    public void deleteShangJia(Shangjia shangjia) throws DaoException, ServiceException {
        shangJiaDao.deleteShangJia(shangjia);
    }

    @Override
    public void updateShangJia(Shangjia shangjia) throws DaoException, ServiceException {
        shangJiaDao.updateShangJia(shangjia);
    }

    @Override
    public Shangjia findShangJiaByName(String shangjianame) throws DaoException, ServiceException {
        return shangJiaDao.findShangJiaByName(shangjianame);
    }

    @Override
    public List findAllShangJia(String hql) throws DaoException, ServiceException {
        return shangJiaDao.findAllShangJia(hql);
    }

    @Override
    public List findAllShangJiaUncheckd(String hql) throws DaoException, ServiceException {
        return shangJiaDao.findAllShangJiaUnchecked(hql);
    }
}
