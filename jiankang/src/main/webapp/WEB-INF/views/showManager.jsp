<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2014/9/14
  Time: 9:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>显示管理员</title>
</head>
<body>
    <center>
        <%@include file="manageTop.jsp"%>
        <br>
        <c:choose>
            <c:when test="${sessionManage == null }">
            <table align="center" width="80%" >
                <tr align="center">
                    <td>
                        <h2><a href="<c:url value="/Manage/LoginPage"/>"/>操作错误，请先登录！ </h2>
                    </td>
                </tr>
            </table>
            </c:when>
            <c:when test="${sessionManage!= null && sessionManage.manageLevel == 1}">
                <table align="center" width="100%" cellspacing="1" border="2">
                    <tr align="center" bgcolor="#fffafa">
                        <td>管理员ID</td>
                        <td>管理员级别</td>
                        <td>管理员名字</td>
                        <td>操作</td>
                    </tr>
                    <c:forEach var="manageList" items="${manageList}" varStatus="status">
                        <tr align="center" >
                            <td><c:out value="${manageList.manageId}"/></td>
                            <td>
                                <c:if test="${manageList.manageLevel==1}">
                                    <c:out value="超级管理员"/>
                                </c:if>
                                <c:if test="${manageList.manageLevel==0}">
                                    <c:out value="普通管理员"/>
                                </c:if>
                            </td>
                            <td>
                                <c:out value="${manageList.manageName}"/>
                            </td>
                            <td>


                                <a href="<c:url value="/Manage/delete?manageName=${manageList.manageName}"/> "/>删除
                                &nbsp;&nbsp;
                                <a href="<c:url value="/Manage/updatePage?manageName=${manageList.manageName}"/> "/>修改


                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <br>
                <table align="center">
                    <tr align="center">
                        <td><a href="<c:url value=""/>"/> 添加管理员 </td>
                    </tr>
                </table>
            </c:when>
            <c:otherwise>
                <table align="center">
                    <tr align="center">
                        <td>
                            <h2>操作错误，权限不够！</h2>
                        </td>
                    </tr>
                </table>
            </c:otherwise>

        </c:choose>
    </center>
</body>
</html>
