package com.healthy.service;

import com.healthy.domain.Shangjia;
import com.healthy.exception.DaoException;
import com.healthy.exception.ServiceException;

import java.util.List;

/**
 * Created by dell on 2014/9/16.
 */
public interface ShangJiaService {
    public void saveShangJia(Shangjia shangjia) throws DaoException,ServiceException;
    public void deleteShangJia(Shangjia shangjia) throws DaoException,ServiceException;
    public void updateShangJia(Shangjia shangjia) throws DaoException,ServiceException;
    public Shangjia findShangJiaByName(String shangjianame) throws DaoException,ServiceException;
    public List findAllShangJia(String hql)throws DaoException,ServiceException;
    public List findAllShangJiaUncheckd(String hql)throws DaoException,ServiceException;
}
