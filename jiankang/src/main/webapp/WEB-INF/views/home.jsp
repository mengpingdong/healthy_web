<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2014/9/11
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
    <title>后台首页</title>
    <Script language="JavaScript">
        function Check1(){
            if(document.shangjialoginpage.shangjiaName.value == ""){
                alert("请填写用户名！！！");
                document.shangjialoginpage.shangjiaName.focus();
                return false;
            }
            if(document.shangjialoginpage.shangjiaPwd.value == ""){
                alert("请填写密码！！！");
                document.shangjialoginpage.shangjiaPwd.focus();
                return false;
            }
            document.shangjialoginpage.submit();
        }
    </Script>
</head>
<body>
    <%@include file="hometop.jsp"%>
    <c:choose>
        <c:when test="${shangjia_session == null}">
            <table id="back" align="center">
                <form name="shangjialoginpage" action="<c:url value="/ShangJia/Login" />" method="post">
                    <tr height=80><td height="53"></td></tr>
                    <tr>
                        <td width="30%" height="55" align="right">用户名：</td>
                        <td colspan="2" width="70%">
                            <input type="text" name="shangjiaName" size="50"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" height="57" align="right">密码:</td>
                        <td colspan="2" width="70%">
                            <input type="password" name="shangjiaPwd" size="20"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <input type="button" size="20" value="登录" onclick="Check1()">
                        </td>
                        <td align="left">
                            &nbsp;&nbsp;
                            <input type="reset" size="20" value="重置">
                        </td>
                    </tr>
                </form>
            </table>
        </c:when>
        <c:when test="${shangjia_session != null}">
            <table align="center" >
                <tr align="center">
                    <td>
                        <h2>对不起，您已登录！！！<a href="<c:url value="/ShangJia/Logout"/> ">注销</a> </h2>
                    </td>
                </tr>
            </table>
        </c:when>
    </c:choose>
    <br>
    <br>
    <c:if test="${not empty shangjia_error}">
        <table align="center" width="60%" height="60%">
            <tr align="center">
                <td>
                    <h2>${shangjia_error}</h2>
                </td>
            </tr>
            <c:remove var="shangjia_error"/>
        </table>
    </c:if>
</body>
</html>
