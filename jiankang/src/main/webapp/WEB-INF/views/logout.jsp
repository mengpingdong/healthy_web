<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2014/9/12
  Time: 14:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>logout</title>
</head>
<body>
<%@include file="hometop.jsp"%>
<br><br>
<c:if test="${logout_error != null}">
    <table align="center" width="100%" height="100%">
        <tr align="center">
            <td>
                <h2>注销出错:</h2>${logout_error}
            </td>
        </tr>
        <c:remove var="logout_error" />
    </table>
</c:if>
<c:if test="${login_error == null}">
    <table align="center" width="100%" height="100%">
        <tr align="center">
            <td>
                <h2>注销成功 ！</h2>
            </td>
        </tr>
    </table>
</c:if>
</body>
</html>
